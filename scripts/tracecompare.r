library(tidyverse)
library(dplyr)
library(magrittr)

#!/usr/bin/env Rscript
argas <- commandArgs(TRUE)
csv1 = paste ("tsubasa.time.og.11.csv",sep="")
csv2 = paste ("tsubasa.time.new.11.csv",sep="")
pdf = paste ("comparisonnewvsog.time.",argas[1],".pdf",sep="")
pdf2 = paste ("comparisonnewvsog.cache.",argas[1],".pdf",sep="")
pdf3 = paste ("comparisonnewvsog.flops.",argas[1],".pdf",sep="")
pdf4 = paste ("comparisonnewvsog.taxavet.",argas[1],".pdf",sep="")
pdfper = paste ("comparisonnewvsog.time.percentage",argas[1],".pdf",sep="")


df <- read_csv(csv1, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X1, Size = X2, Duration = X3, Cache = X4, Flops = X5, Vet = X6, Secache = X7, Sevet = X8, Seflops = X9) %>%
        select(-contains("X")) %>%
        print

ncol1 <- character(length(df$Benchmark))
for(i in 1:length(df$Benchmark)) {
ncol1[i] <- 'original'
}
df$Versão <- ncol1

df2 <- read_csv(csv2, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X1, Size = X2, Duration = X3, Cache = X4, Flops = X5, Vet = X6, Secache = X7, Sevet = X8, Seflops = X9) %>%
        select(-contains("X")) %>%
        print

ncol2 <- character(length(df2$Benchmark))
for(i in 1:length(df2$Benchmark)) {
ncol2[i] <- 'otimizado'
}
df2$Versão <- ncol2

total <- merge(df,df2,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet", "Secache", "Seflops", "Sevet", "Versão"), all= TRUE)

print(total)


if (argas[1] != 'all'){
        total <- total %>% filter(Benchmark == argas[1])
}
        
print(total)

total %>%
        ggplot(aes(x = Size, y = Flops, fill = Versão)) +
        theme_bw(base_size=16) +
        geom_col(position="dodge") +
        geom_errorbar(aes(x=Size, ymin=Flops-Seflops, ymax=Flops+Seflops), width=5.9, colour="black", alpha=0.8, size=0.8, position = position_dodge(29)) +
        scale_fill_brewer(palette = "Set1") +
        xlab("Tamanho de entrada N X N") +
        ylab("MFLOPS") +
        theme_bw() +
        theme(axis.text=element_text(size=28), axis.title=element_text(size=32), legend.title = element_text(size = 30), legend.text = element_text(size = 28))
        
ggsave(pdf3, width=16, height=9)

total %>%
        ggplot(aes(x = Size, y = Vet, fill = Versão)) +
        theme_bw(base_size=16) +
        geom_col(position="dodge") +
        geom_errorbar(aes(x=Size, ymin=Vet-Sevet, ymax=Vet+Sevet), width=5.9, colour="black", alpha=0.8, size=0.8, position = position_dodge(29)) +
        scale_fill_brewer(palette = "Set1") +
        xlab("Tamanho de entrada N X N") +
        ylab("Taxa de operações vetoriais [%]") +
        theme_bw() +
        theme(axis.text=element_text(size=28), axis.title=element_text(size=32), legend.title = element_text(size = 30), legend.text = element_text(size = 28))
        
ggsave(pdf4, width=16, height=9)