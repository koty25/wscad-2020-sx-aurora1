library(tidyverse)
library(dplyr)
library(magrittr)

#!/usr/bin/env Rscript

argas <- commandArgs(TRUE)

csv1 = paste ("times.ncc.csv",sep="")


df <- read_csv(csv1, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X1, Cache = X2, Flops = X3, Vet = X4) %>%
        select(-contains("X")) %>%
        print

rc <- df %>%
        group_by(Benchmark) %>%
        summarize(Mean.Cache = sum(Cache)/lenght(Cache), Mean.Flops = sum(Flops)/lenght(Flops), Mean.Vet = sum(Vet)/lenght(Vet), Se.Cache = sd(Cache)/sqrt(lenght(Cache)), Se.Flops = sd(Flops)/sqrt(lenght(Flops)), Se.Vet = sd(Vet)/sqrt(lenght(Vet)))
        write.csv(rc,"./mean.csv", row.names = FALSE)