library(tidyverse)
library(dplyr)
library(magrittr)

#!/usr/bin/env Rscript
argas <- commandArgs(TRUE)
csv1 = paste (argas[1], ".csv",sep="")
pdf1 = paste (argas[1], ".png",sep="")



df <- read_csv(csv1, col_names=FALSE, col_types=cols()) %>%
        rename( Implementação = X1, Duration = X2, Entrada = X3) %>%
        select(-contains("X")) %>%
        arrange(desc(Duration)) %>%
        print

df <- df[with(df, order(Entrada, -Duration)), ]



df$Implementação <- factor(df$Implementação, levels = df$Implementação[order(-df$Duration)])

print(df)

df %>%
        ggplot(aes(x = Entrada, y = Duration, fill = Implementação)) +
        theme_bw(base_size=16) +
        geom_bar(position="dodge", stat="identity", width=1) +
        scale_fill_brewer(palette = "Set1") +
        xlim(2047,2049) +
        xlab("Entrada 2048 X 2048") +
        ylab("Tempo [s]") +
        theme_bw()+
        theme(axis.text=element_text(size=14), axis.title=element_text(size=32), legend.title = element_text(size = 28), legend.text = element_text(size = 24), axis.text.x = element_blank())
        
        
ggsave(pdf1, width=16, height=9)
