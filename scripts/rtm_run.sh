#!/bin/bash
#SBATCH --exclusive
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=tsubasa
#SBATCH --time=2:00:00
#SBATCH --job-name=cmp270-jobrtm-trace1otl
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err

root=`pwd`
export root
host=`hostname | awk -F. {'print $1'}`
arch=`gcc -march=native -Q --help=target | grep march | awk '{print $2}'`

date +"%d/%m/%Y %H:%M:%S"
printf "\t Running on $arch@$host \n\n"

date +"%d/%m/%Y %H:%M:%S"

exp=exp_3_og_trace1fopenmploopunroll

expdir=$root/experiments/$exp
if [ ! -d $expdir ]; then
 mkdir $expdir
fi

source env.sh
export VE_PROGINF=DETAIL
export VE_PERF_MODE=VECTOR-OP
export OMP_NUM_THREADS=8
export OMP_FLAG=-fopenmp

./compile.sh &>> $expdir/rtm-make.out
printf "\n"

rm -rf $SCRATCH/bin/
cp -r $root/bin/ $SCRATCH/

cd /tmp

while true; do
	step=`ls $expdir | grep $host | tail -1 | awk -F. {'print $3'}`
	if [ -z "$step" ]; then
		step=0
	fi
	
	doe=$root/DoE/$host.$exp.csv
	if [ -f "$doe" ]; then
	    printf "\t Using old $doe\n"
	else 
	    $root/DoE.sh 1 $exp
	    step=$((step+1))
	fi
	log=$expdir/$host.$step.log
	outputtime=$expdir/$host.time.$step.csv
	outputsample=$expdir/$host.sample.$step.csv

	printf "\n"

	printf "\t Step: $step \n\n"

	while IFS=\; read -r app version size; do			
		date +"%d/%m/%Y %H:%M:%S"
		printf "\t Application: $app \n"
		printf "\t Version: $version \n"
		printf "\t Size: $size \n\n"
		
		exec=$SCRATCH/bin/$app.$version.$host.x

		if [[ $version == *"OpenACC-GPU"* ]]; then
			unset -v ACC_NUM_CORES
			export ACC_DEVICE_TYPE=nvidia
	  		$exec TTI $size $size $size 16 12.5 12.5 12.5 0.001 0.1 > /tmp/fletcher.out 2> /tmp/fletcher.err
	  		export ACC_DEVICE_TYPE=host
			export ACC_NUM_CORES=`lscpu | grep "^CPU(s):" | awk {'print $2'}`
		else
			$exec TTI $size $size $size 16 12.5 12.5 12.5 0.001 0.1 > /tmp/fletcher.out 2> /tmp/fletcher.err
		fi

		SAMPLES=`cat /tmp/fletcher.out | grep Samples | awk {'print $2'}`
		TIME=`cat /tmp/fletcher.out | grep "Execution time (s) is" | awk {'print $5'}`
		FLOPS=`cat /tmp/fletcher.out | grep "MFLOPS                                  :" | awk {'print $3'} >> $expdir/times.${cf}.csv`
      	VET=`cat /tmp/fletcher.out | grep "V. Op. Ratio (%)                        :" | awk {'print $6'} >> $expdir/times.${cf}.csv`
      	CACHE=`cat /tmp/fletcher.out | grep "VLD LLC Hit Element Ratio (%)           :" | awk {'print $8'} >> $expdir/times.${cf}.csv`

		cat /tmp/fletcher.out >> $log
		cat /tmp/fletcher.err >> $log

		echo $host,$arch,$app,$version,$size,"samples",$SAMPLES,"M", CACHE, FLOPS, VET >> $outputsample
		echo $host,$arch,$app,$version,$size,"time",$TIME,"s", CACHE, FLOPS, VET >> $outputtime
		mv ./ftrace.out $expdir/output/ftrace.$app.$version.$size.out

		sed -i '1d' $doe
                find $DOE -size 0 -delete
	done < $doe

	date +"%d/%m/%Y %H:%M:%S"
	printf "\t done - $output \n\n"

	rm -f $doe
done
