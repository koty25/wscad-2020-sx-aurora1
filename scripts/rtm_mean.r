library(tidyverse)
library(dplyr)
library(magrittr)

#!/usr/bin/env Rscript

argas <- commandArgs(TRUE)

csv1 = paste ("tsubasa.time.og.1.csv",sep="")
csv2 = paste ("tsubasa.time.og.2.csv",sep="")
csv3 = paste ("tsubasa.time.og.3.csv",sep="")
csv4 = paste ("tsubasa.time.og.4.csv",sep="")
csv5 = paste ("tsubasa.time.og.5.csv",sep="")
csv6 = paste ("tsubasa.time.og.6.csv",sep="")
csv7 = paste ("tsubasa.time.og.7.csv",sep="")
csv8 = paste ("tsubasa.time.og.8.csv",sep="")
csv9 = paste ("tsubasa.time.og.9.csv",sep="")
csv10 = paste ("tsubasa.time.og.10.csv",sep="")



df <- read_csv(csv1, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df2 <- read_csv(csv2, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df3 <- read_csv(csv3, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df4 <- read_csv(csv4, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df5 <- read_csv(csv5, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df6 <- read_csv(csv6, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df7 <- read_csv(csv7, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df8 <- read_csv(csv8, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df9 <- read_csv(csv9, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print
df10 <- read_csv(csv10, col_names=FALSE, col_types=cols()) %>%
        rename(Benchmark = X3,Size = X5, Duration = X7, Cache = X9, Flops = X10, Vet = X11) %>%
        select(-contains("X")) %>%
        print

total <- merge(merge(merge(merge(merge(merge(merge(merge(merge(merge(
	df,df2,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df3,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df4,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df5,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df6,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df7,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df8,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df9,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE),
	df10,by=c("Benchmark","Size", "Duration", "Cache", "Flops", "Vet"), all= TRUE)

rc <- total %>%
        group_by(Benchmark, Size, Duration) %>%
        summarize(Mean.Cache = sum(Cache)/lenght(Cache), Mean.Flops = sum(Flops)/lenght(Flops), Mean.Vet = sum(Vet)/lenght(Vet), Se.Cache = sd(Cache)/sqrt(lenght(Cache)), Se.Flops = sd(Flops)/sqrt(lenght(Flops)), Se.Vet = sd(Vet)/sqrt(lenght(Vet)))
        write.csv(rc,"./mean.csv", row.names = FALSE)