#!/bin/bash
#SBATCH --exclusive
#SBATCH --nodes=1
#SBATCH --ntasks=8
#SBATCH --partition=tsubasa
#SBATCH --time=2:00:00
#SBATCH --job-name=cmp270-jobmatrix1
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err


export PATH=/opt/nec/ve/ncc/3.0.4/bin:$PATH
export VE_PROGINF=DETAIL
export VE_PERF_MODE=VECTOR-OP

rm bin/mm*

for i in 4096; do

ncc -O2 -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.c -o ./bin/mm &>> make_final.txt
ncc -O2 -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.lit.c -o ./bin/mm.lit &>> make_final.txt
ncc -O2 -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.liturl.c -o ./bin/mm.liturl &>> make_final.txt
ncc -O2 -fopenmp -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.litomp.c -o ./bin/mm.litomp &>> make_final.txt
ncc -O2 -fopenmp -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.litompurl.c -o ./bin/mm.litompurl &>> make_final.txt
ncc -O2 -fopenmp -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.litomptil.c -o ./bin/mm.litomptil &>> make_final.txt
ncc -O2 -fopenmp -report-all -fdiag-inline=2 -fdiag-parallel=2 -fdiag-vector=2 mm.litompurltil.c -o ./bin/mm.litompurltil &>> make_final.txt


root=`pwd`
output=$root/final_final.csv
touch $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="ingenuo"

echo $prog,$time,$i >> $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm.lit $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="loopinterchange"

echo $prog,$time,$i >> $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm.liturl $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="loop_inter+unroll"

echo $prog,$time,$i >> $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm.litomp $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="loop_inter+openmp"

echo $prog,$time,$i >> $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm.litompurl $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="loop_inter+unroll+openmp"

echo $prog,$time,$i >> $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm.litomptil $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="tiling"

echo $prog,$time,$i >> $output

exec 3>&1 4>&2
foo=$( { time ./bin/mm.litompurltil $i 1>&3 2>&4; } 2>&1 )
exec 3>&- 4>&-

time=`echo $foo | awk  '{print $2}' | sed 's/^0m//;s/s$//'`
prog="unroll_tiling"

echo $prog,$time,$i >> $output

done

cat $output
